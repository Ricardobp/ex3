package eX3;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Scanner;

public class App {
    Produto produto = new Produto();

    private LinkedList<Produto> produtos = new LinkedList<Produto>();

    public void executar(int opcao) throws ParseException {
        switch(opcao){
            case 1:
                addProduto();
                break;
            case 2:
                listaOrçamento();
                break;
            case 3:
                break;
            default:
                System.out.println("% Escolha uma opcao valida %");
        }
    }

    public void addProduto() throws ParseException {
        Scanner sc = new Scanner(System.in);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        System.out.print("Nome do Produto: ");
        produto.setNomeProduto(sc.nextLine());
        System.out.print("Quantidade: ");
        produto.setQuantidade(sc.nextInt());
        System.out.print("Preço: ");
        produto.setPreco(sc.nextDouble());

        System.out.println("Padrão -> dd/MM/yyyy");
        System.out.println();
        System.out.print("Data da compra: ");
        Date dinicio = df.parse(sc.next());

        Produto produto = new Produto();
        produto.setDataCompra(dinicio);
        produtos.add(produto);

        System.out.println("+----------------+");
        System.out.println("| Lista Guardada |");
        System.out.println("+----------------+");
    }

    public void mostrarTela() throws ParseException {
        Scanner sc = new Scanner(System.in);
        int opcao = 0;
        do{
            System.out.println();
            System.out.println("Lista de Compras");
            System.out.println("------------------------");
            System.out.println("1. Adicionar Produto");
            System.out.println("2. Listar Orçamento");
            System.out.println("3. Sair");
            System.out.println("------------------------");
            System.out.print("Opcao: ");
            opcao = sc.nextInt();
            executar( opcao );
        }while(opcao != 3 );
    }

    public void listaOrçamento(){
        for (Produto produt : produtos){
            produt.getQuantidade();
            produt.getPreco();
            produt.getDataCompra();
            produt.getNomeProduto();
        }
    }


    public static void main(String[] args) throws ParseException {
        App app = new App();
        app.mostrarTela();
    }
}
