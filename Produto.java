package eX3;

import java.util.Date;

public class Produto {

    public String nomeProduto;
    public int quantidade;
    public double preco;
    public Date DataCompra;


    public Date getDataCompra() {
        return DataCompra;
    }

    public void setDataCompra(Date dataCompra) {
        DataCompra = dataCompra;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    @Override
    public String toString(){
        return "Nome: \n" + getNomeProduto() + "Data: \n" + getDataCompra() + "Preco: \n" + getPreco() + "Quantidade: " + getQuantidade();
    }

}
